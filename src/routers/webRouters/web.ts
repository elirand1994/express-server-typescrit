import express, { RequestHandler } from 'express';
var router = express.Router();
import log from '@ajar/marker';

const functionLogger : RequestHandler = (req : express.Request,res : express.Request,next: Function) =>{
    log.cyan('Only logs at adding new user..')
    next();
}
router.use(functionLogger);
router.post('/newUser',(req,res)=>{
    const {user_id,password,email} = req.body 
    const responseObj = {
        user_id : user_id,
        password : password,
        email : email
    }
    res.status(200).send(responseObj)
})

router.get('/search',(req,res)=>{
    const food = req.query.food;
    const town = req.query.town;
    const responseObj = {
        food : food,
        town : town
    }
    res.status(200).send(responseObj);
})

router.get('/search/:id', (req,res)=>{
    const id = req.params.id;
    res.status(200).send(id);
})

export default router;